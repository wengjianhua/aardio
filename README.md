![演示](https://gitee.com/wengjianhua/aardio/raw/master/%E6%BC%94%E7%A4%BA%E5%9B%BE%E7%89%87.png "例子")

# aardio plus-element-css 

#### 介绍
plus-element-css ，适用于plus控件的仿element的样式库

用了这个库好处是，拉一个plus控件，再加一句代码就可设置完成一个控件，快速简单漂亮大方......

#### 使用说明

1.将“css.aardio”文件复制到 “E:\aardio\lib\win\ui” 文件夹

2.Import win.ui.css 导入库文件，详细看例子


